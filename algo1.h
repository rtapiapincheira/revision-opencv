#ifndef __ALGO1_H_
#define __ALGO1_H_

#include "cvlib.h"

bool checkTriangle(const Point &p1, const Point &p2, const Point &p3, double A, double B);

vector<Point> makeVector(const Point &p1, const Point &p2, const Point &p3);

bool orderedPoints(const Point &p1, const Point &p2, const Point &p3);

bool equalByPercent(double val1, double val2, double allowedPercent);

Point removeShortestTo(vector<Point> &res, const Point &p);

void orderByShortestDistanceToCorners(vector<Point> &res, const Size &wa);

string infoToString(MyBlob blob, double absolutePercent, double boundingPercent);

void getCalculatedPositions(const Mat &src, vector<MyBlob> &blobs);

Mat drawGrid(const Mat &src);

Mat drawLines(const Mat &src, const vector<Point> &points, const Scalar &color) ;

vector<Point> getTriangulatedPoints(const Point &p1, const Point &p2, const Point &p3, const Point &p4, const Size &wa);

vector<Point> findPoints(const vector<MyBlob> &points, const Size &workArea);

Mat showWarp(const Mat &src, const vector<Point> &trapezoid);

int processImage(const Mat &_image, const string &suffix);

int process_algo1(int argc, char **argv);

#endif // __ALGO1_H_
