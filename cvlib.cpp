#include "cvlib.h"

MyBlob::MyBlob() {
}

MyBlob::MyBlob(int _id, int x, int y, int _area, Rect _rect) :
    id(_id), p(x, y), area(_area), rect(_rect)
{
}

double getDist(const Point &p1, const Point &p2) {
    double d1 = p1.x - p2.x;
    double d2 = p1.y - p2.y;
    return sqrt(d1*d1 + d2*d2);
}

Point getMassCenter(const vector<Point> &points) {
    double totalX = 0.0;
    double totalY = 0.0;

    int n = points.size();
    for(int i = 0; i < n; i++) {
        totalX += (points[i].x / (double)n);
        totalY += (points[i].y / (double)n);
    }

    return Point((int)totalX, (int)totalY);
}

Mat resizeToMatch(const Mat &src, float maxDim) {
    Size dim = src.size();
    int maxVal = (dim.width > dim.height ? dim.width : dim.height);
    float times = maxVal / maxDim;
    int newW = dim.width / times;
    int newH = dim.height / times;
    Size size(newW, newH);
    Mat dst;
    resize(src, dst, size);
    return dst;
}

#if USE_WINDOWS
int myShowImage(const Mat &img, const string &name) {
    cvNamedWindow(name.c_str(), WINDOW_AUTOSIZE);
    IplImage tmp = img;
    cvShowImage(name.c_str(), &tmp);
    //waitKey(0);
    return 0;
}
#endif

Mat filterImage(const Mat &src, int i) {
    Mat dst;
    // Normalized Block Filter: (5) [1,21]
    blur(src, dst, Size(i, i), Point(-1,-1));

    // Gaussian Filter: (9) [1,3,5,...]
    //GaussianBlur(src, dst, Size(2*i+1, 2*i+1), 0, 0);

    // Median Filter: (11)
    //medianBlur(src, dst, 2*i+1);

    // Bilateral Filter: (21)
    //bilateralFilter(src, dst, i, i*2, i/2);

    return dst;
}

Mat myLoadImage(const string &filename, int loadMode) {
    return Mat(cvLoadImage(filename.c_str(), loadMode));
}

string int2str(int val) {
    char buff[50];
    snprintf(buff, sizeof(buff), "%d", val);
    return string(buff);
}

void show(const Mat &img, const string &name) {
#if USE_WINDOWS
    myShowImage(resizeToMatch(img, 650), name);
#endif
}

bool massCenterForBlob(const Mat &_src, MyBlob &blob, uchar magicColor) {
    Mat src = _src;
    int a = floodFill(src, blob.p, Scalar(magicColor));
    if (a == 0) {
        return false;
    }
    int width = src.size().width;
    int height = src.size().height;
    double sum_x = 0;
    double sum_y = 0;
    int sum = 0;
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            if (src.at<uchar>(y,x) == magicColor) {
                sum_x += x / (double) a;
                sum_y += y / (double) a;
                sum++;
            }
        }
    }
    blob.massCenter = Point((int)sum_x, (int)sum_y);
    return true;
}

void getBlobs(Mat &image, vector<MyBlob> &blobs, uchar color, uchar magicColor) {
  Size dim = image.size();
  blobs.clear();

  for (int y = 0; y < dim.height; y++) {
    for (int x = 0; x < dim.width; x++) {
      if (image.at<uchar>(y,x) == color) {

        Point p(x, y);
        Rect rect;
        int a = floodFill(image, p, Scalar(magicColor), &rect);

        blobs.push_back(MyBlob(0, x, y, a, rect));
      }
    }
  }
}
