#ifndef __CVLIB_H_
#define __CVLIB_H_

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>

#include <stdio.h>
#include <iostream>
#include <algorithm>

using namespace std;
using namespace cv;

#define USE_CMD     0
#define USE_COUT    1
#define USE_WINDOWS 1

class MyBlob {
public:
    int id;
    Point p;
    Point massCenter;
    int area;
    Rect rect;

    MyBlob();

    MyBlob(int _id, int x, int y, int _area, Rect _rect);
};

template <class T>
bool inRange(const T &value, const T &limA, const T &limB) {
    return (limA <= value && value <= limB);
}

double getDist(const Point &p1, const Point &p2);

Point getMassCenter(const vector<Point> &points);

Mat resizeToMatch(const Mat &src, float maxDim = 1000.0f);

#if USE_WINDOWS
int myShowImage(const Mat &img, const string &name="Display window");
#endif

Mat myLoadImage(const string &filename, int loadMode=CV_LOAD_IMAGE_GRAYSCALE);

Mat filterImage(const Mat &src, int i=1);

string int2str(int val);

void show(const Mat &img, const string &name);

bool massCenterForBlob(const Mat &_src, MyBlob &blob, uchar magicColor);

void getBlobs(Mat &image, vector<MyBlob> &blobs, uchar color=0, uchar magicColor=32);

#endif // __CVLIB_H_
