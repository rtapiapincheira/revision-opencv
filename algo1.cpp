#include "algo1.h"

bool checkTriangle(const Point &p1, const Point &p2, const Point &p3, double A, double B) {
    double d12 = getDist(p1, p2);
    double d23 = getDist(p2, p3);

    return (d12 == A && d23 == B);
}

vector<Point> makeVector(const Point &p1, const Point &p2, const Point &p3) {
    vector<Point> v;
    v.push_back(p1);
    v.push_back(p2);
    v.push_back(p3);
    return v;
}

bool orderedPoints(const Point &p1, const Point &p2, const Point &p3) {
    double d12 = getDist(p1, p2);
    double d23 = getDist(p2, p3);
    double d13 = getDist(p1, p3);

    return (d13 > d12 && d13 > d23) && (d23 > d12);
}

bool equalByPercent(double val1, double val2, double allowedPercent) {
    double valMin = min(val1, val2);
    double valMax = min(val1, val2);
    return inRange(
        valMin,
        valMax * (1.0 - allowedPercent),
        valMax * (1.0 + allowedPercent)
    );
}

Point removeShortestTo(vector<Point> &res, const Point &p) {
    int best = 0;
    for (int i = 1; i < 4; i++) {
        if (getDist(res[i], p) < getDist(res[best], p)) {
            best = i;
        }
    }
    Point q = res[best];
    res.erase(res.begin()+best, res.begin()+best);
    return q;
}

void orderByShortestDistanceToCorners(vector<Point> &res, const Size &wa) {
    vector<Point> result;
    result.push_back(removeShortestTo(res, Point(0, 0)));
    result.push_back(removeShortestTo(res, Point(0, wa.height)));
    result.push_back(removeShortestTo(res, Point(wa.width, wa.height)));
    result.push_back(removeShortestTo(res, Point(wa.width, 0)));

    res = result;
}

string infoToString(MyBlob blob, double absolutePercent, double boundingPercent) {
    char buff[1024];
    snprintf(buff, sizeof(buff), "%d (%d,%d):%d, (%.4f, %.4f)", blob.id, blob.p.x, blob.p.y, blob.area, absolutePercent, boundingPercent);
    return string(buff);
}

void getCalculatedPositions(const Mat &src, vector<MyBlob> &blobs) {
    int n = blobs.size();
    for (int i = 0; i < n; i++) {
        massCenterForBlob(src, blobs[i], 200+i);
    }
}

Mat drawGrid(const Mat &src) {
    Mat res;
    cvtColor(src, res, CV_GRAY2RGB);

    rectangle(res, Rect(50, 1050, 670, 2780), Scalar(255, 0, 255), 6);

    rectangle(res, Rect(895, 1050, 670, 2780), Scalar(255, 0, 255), 6);

    rectangle(res, Rect(2095, 1130, 880, 780), Scalar(255, 0, 255), 6);

    return res;
}

Mat drawLines(const Mat &src, const vector<Point> &points, const Scalar &color) {
    Mat res;
    cvtColor(src, res, CV_GRAY2RGB);
    int n = points.size();
    if (n == 4) {
        for (int i = 0; i < n; i++) {
            int j = (i+1) % n;
            Point p1 = points[i];
            Point p2 = points[j];
            line(res, p1, p2, color, 6);
        }

        circle(res, points[0], 30, Scalar(0, 255, 0), 6);
        circle(res, points[1], 30, Scalar(0, 192, 0), 6);
        circle(res, points[2], 30, Scalar(0, 128, 0), 6);
        circle(res, points[3], 30, Scalar(0, 64, 0),  6);

    }
#if USE_COUT
    else if (n != 0) {
        cout << "different than 4 points to draw! ERROR:" << points.size() << endl;
    }
#endif
    return res;
}

vector<Point> getTriangulatedPoints(const Point &p1, const Point &p2, const Point &p3, const Point &p4, const Size &wa) {

    vector<Point> points;
    points.push_back(p1);
    points.push_back(p2);
    points.push_back(p3);
    points.push_back(p4);

    orderByShortestDistanceToCorners(points, wa);

    vector<Point> res;

    double v1 = getDist(points[0], points[1]);
    double v2 = getDist(points[3], points[2]);

    double h1 = getDist(points[1], points[2]);
    double h2 = getDist(points[0], points[3]);

    double d1 = fabs(points[1].y - points[0].y);
    double d2 = fabs(points[2].y - points[3].y);

    if (equalByPercent(v1, v2, 0.4)) {
        if (equalByPercent(h1, h2, 0.4)) {
            if (equalByPercent(d1, d2, 0.4)) {
                /*if (h1<v1 && h2<v1 && h1<v2 && h2<v2) {
                    return points;
                } else {
                    cout << "Condition for verticality not met, h1,h2=" << h1 << "," << h2 << ", v1,v2=" << v1 << "," << v2 << endl;
                }*/
                return points;
            }
#if USE_COUT
            else {
                cout << "Delta Y too different: " << d1 << " vs " << d2 << endl;
            }
#endif
        }
#if USE_COUT
        else {
            cout << "Horizontal lines too different: " << h1 << " vs " << h2 << endl;
        }
#endif
    }
#if USE_COUT
    else {
        cout << "Vertical lines too different: " << v1 << " vs " << v2 << endl;
    }
#endif
    /*double d1 = getDist(p1, p2);
    double d2 = getDist(p2, p3);
    double d3 = getDist(p3, p1);

    double D = max(d1, max(d2, d3));   // diagonal distance
    double a = min(d1, min(d2, d3));   // upper catet
    double b = (d1 + d2 + d3) - D - a; // left catet

    double A = min(a, b);  // longer of the catets
    double B = max(a, b);  // shorter of the catets

    double kV = 172.0 / 236.0;

    //printf("d1=%.3f d2=%.3f d3=%.3f    D=%.3f a=%.3f b=%.3f    A= %.3f B=%.3f kV=%.3f\n", d1, d2, d3,    D, a, b,    A, B, kV);

    // If there's space
    if (A >= 1.0 && B >= 1.0) {
        //cout << "Passed first test for catet length" << endl;

        // Check whether the ratio is in an expected interval
        double V = A / B;
        //printf("V=%.3f kV=%.3f\n", V, kV);

        if (kV*0.90 <= V && V <= kV*1.1) {

            //cout << "Passed second test for catet ratio!" << endl;

            // Check whether the diagonal respects the ratio (i.e. along
            // with other check, it's right angle)
            const double T = 0.58898583616;

            double U = A / sqrt(A*A + B*B);
            //printf("U=%.3f\n", U);

            if (T*0.90 <= U && U <= T*1.1) {
                //cout << "Passed third test for diagonal length" << endl;

                if (orderedPoints(p1, p2, p3)) { return makeVector(p1, p2, p3); }
                if (orderedPoints(p1, p3, p2)) { return makeVector(p1, p3, p2); }
                if (orderedPoints(p2, p1, p3)) { return makeVector(p2, p1, p3); }
                if (orderedPoints(p2, p3, p1)) { return makeVector(p2, p3, p1); }
                if (orderedPoints(p3, p1, p2)) { return makeVector(p3, p1, p2); }
                if (orderedPoints(p3, p2, p1)) { return makeVector(p3, p2, p1); }

                return res;
            }
        }
    }*/

    return res;
}

vector<Point> findPoints(const vector<MyBlob> &points, const Size &workArea) {
    vector<Point> res;
    if (4 <= points.size()) {
        int n = points.size();
        int r = 4;

        vector<bool> v(n);
        fill(v.begin(), v.end() - n + r, true);

        Point ps[4];
        int pIndex = 0;
        do {
            for (int i = 0; i < n; ++i) {
               if (v[i]) {
                   ps[pIndex++] = points[i].massCenter;
               }
            }
            vector<Point> found = getTriangulatedPoints(ps[0], ps[1], ps[2], ps[3], workArea);
            if (!found.empty()) {
                res.clear();

                res.push_back(found[3]);
                res.push_back(found[0]);
                res.push_back(found[1]);
                res.push_back(found[2]);

                orderByShortestDistanceToCorners(res, workArea);
#if USE_COUT
                cout << "Solution found!" << endl;
#endif
                return res;
            }
#if USE_COUT
            else {
                cout << "Solution NOT found!" << endl;
            }
#endif
            pIndex = 0;

        } while (std::prev_permutation(v.begin(), v.end()));
    }
    return res;
}

Mat showWarp(const Mat &src, const vector<Point> &trapezoid) {
    Point2f subRect[4];
    for (int i = 0; i < 4; i++) {
        subRect[i] = trapezoid[i];
    }

    Point2f fullRect[4];

    fullRect[1] = Point2f(0, src.size().height);
    fullRect[2] = Point2f(src.size().width, src.size().height);
    fullRect[3] = Point2f(src.size().width, 0);
    fullRect[0] = Point2f(0, 0);

    Mat warp;
    Mat transformation = getPerspectiveTransform(subRect, fullRect);
    warpPerspective(src, warp, transformation, Size(0, 0));

    return warp;
}

int processImage(const Mat &_image, const string &suffix) {
    Mat image(_image);

    if (!image.data) {
        cout << "Could not open or find the image" << endl ;
        return -1;
    }
    Mat filtered1 = filterImage(image, 11);
    //show(filtered1, suffix + "filtered");

    Mat thresholded;
    threshold(filtered1, thresholded, 80, 255, THRESH_BINARY);
    //show(thresholded, suffix + "thresholded");

    vector<MyBlob> blobs;
    getBlobs(thresholded, blobs);

    int width = image.size().width;
    int height = image.size().height;
    int totalPixels = width * height;

    Mat processed = thresholded;

    vector<MyBlob> filteredBlobs;

    int n = blobs.size();
    for(int i = 0; i < n; i++) {
        double absolutPercent = blobs[i].area / (double) totalPixels;
        int boundingArea = blobs[i].rect.width * blobs[i].rect.height;
        double boundingPercent = blobs[i].area / (double) boundingArea;

        int w = blobs[i].rect.width;
        int h = blobs[i].rect.height;

        int minDim = (w < h ? w : h);
        int maxDim = (w+h-minDim);

        double ratio = minDim / (double)maxDim;

#if USE_COUT
        cout << "area #" << (i+1) << ":" << blobs[i].area << endl;
#endif

        if (ratio >= 0.70 && boundingPercent >= 0.70 && /*600 <= blobs[i].area*/ inRange(blobs[i].area, 100, 300)) {
            filteredBlobs.push_back(blobs[i]);
        } else {
            floodFill(processed, blobs[i].p, Scalar(224));
        }
    }

#if USE_COUT
    cout << "found " << filteredBlobs.size() << "/" << n << " blobs" << endl;
#endif

    show(thresholded, suffix + "processed");

    // Calculate mass-center for each blob
    getCalculatedPositions(processed, filteredBlobs);

    // Try getting 4 points that define the sheet
    vector<Point> trapezoid = findPoints(filteredBlobs, image.size());

#if USE_COUT
    cout << "trapezoid:" << trapezoid.size() << endl;
#endif

    if (!trapezoid.empty()) {

        cout << "{\"warp\":";
        cout << "[";
        for (int i = 0; i < trapezoid.size(); i++) {
            if (i > 0) {
                cout << ",";
            }
            Point p = trapezoid[i];
            double cx = p.x / (double)width;
            double cy = p.y / (double)height;

            cout << cx << "," << cy;
        }
        cout << "]}" << endl;

        Mat warp = showWarp(image, trapezoid);

        //show(warp, suffix + "warp");

        warp = drawGrid(warp);

        show(warp, suffix + "warp_grid");
    } else {
        cout << "{\"error\":\"no trapezoid found\"}";
    }

    Mat result = drawLines(image, trapezoid , Scalar(255, 0, 0));
    show(result, suffix + "result");

    return 0;
}

int process_algo1(int argc, char **argv) {
#if USE_CMD
    if (argc < 2) {
#if USE_COUT
        cout << "Should specify a filename to be processed" << endl;
#endif
        return 1;
    }
    //*
    processImage(Mat(cvLoadImage(argv[1], CV_LOAD_IMAGE_GRAYSCALE)), "pic9_");
    //processImage(Mat(cvLoadImage("images/pic10.jpg", CV_LOAD_IMAGE_GRAYSCALE)), "pic10_");

#if USE_WINDOWS
    waitKey(100);
#endif
    //*/
#else
    ///*
    VideoCapture cap;
    if(!cap.open(0)) {
#if USE_COUT
        cout << "can't open camera" << endl;
#endif
        return 0;
    }
    while (true) {
        Mat frame;
        cap >> frame;
        //transpose(frame, frame);

        if( frame.empty() ) {
#if USE_COUT
            cout << "empty frame" << endl;
#endif
            break;
        }
        cvtColor(frame, frame, CV_RGB2GRAY);
        if (processImage(frame, "line_") < 0) {
#if USE_COUT
            cout << "bad image processing" << endl;
#endif
            break;
        }
        if(waitKey(1) == 27) {
#if USE_COUT
            cout << "ESC event detected" << endl;
#endif
            break;
        }
    }
    //*/
#endif
    return 0;
}

