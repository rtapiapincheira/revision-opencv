#include "algo2.h"

int process_algo2(int argc, char **argv) {
    VideoCapture cap;
    if (!cap.open(0)) {
        cout << "can't open camera" << endl;
        return 0;
    }

    cvNamedWindow("test", WINDOW_AUTOSIZE);

    int times = 0;

    while (true) {
        Mat frame;
        cap >> frame;

        if(frame.empty()) {
            cout << "empty frame" << endl;
            break;
        }
        cvtColor(frame, frame, CV_RGB2GRAY);

        IplImage tmp = frame;
        cvShowImage("test", &tmp);

        int key = waitKey(1);
        if (key == 27) {
            break;
        } else if (key != -1) {
            string name = "imgxx.jpg";
            name[3] = '0' + (times/10);
            name[4] = '0' + (times%10);
            cvSaveImage(name.c_str(), &tmp);
            times++;
        }
    }
    return 0;
}
