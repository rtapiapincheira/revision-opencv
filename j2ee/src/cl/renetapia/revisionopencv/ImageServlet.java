package cl.renetapia.revisionopencv;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.misc.BASE64Decoder;

public class ImageServlet extends HttpServlet {

    private static SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd'T'hhmmss");

    private static final long serialVersionUID = 1L;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            StringBuffer buffer = new StringBuffer();
            Reader reader = request.getReader();
            int current;
            while ((current = reader.read()) >= 0) {
                buffer.append((char) current);
            }
            String data = new String(buffer);
            data = data.substring(data.indexOf(",") + 1);

            //System.out.println("PNG image data on Base64: " + data);

            String filename = "/Users/rene/Desktop/revision-opencv/j2ee/img_temp.png";
            FileOutputStream output = new FileOutputStream(new File(filename));
            output.write(new BASE64Decoder().decodeBuffer(data));
            output.flush();
            output.close();


            response.setContentType("application/json");
            PrintWriter pw = new PrintWriter(response.getOutputStream());

            String out = getExecOutput("/Users/rene/Desktop/revision-opencv/cmd_main", filename);
            pw.println(out);

            System.out.println("#" + out);

            pw.flush();

            //sendImage(filename, response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String []args) throws Exception  {
        System.out.println(getExecOutput("/Users/rene/Desktop/revision-opencv/cmd_main", "/Users/rene/Desktop/revision-opencv/j2ee/img_temp.png"));
        //System.out.println(getExecOutput("ls", "-1"));
    }

    public static String getExecOutput(String command, String arg) {
        try {
            // Use a ProcessBuilder
            ProcessBuilder pb = new ProcessBuilder(command, arg);

            Process p = pb.start();
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }
            int r = p.waitFor(); // Let the process finish.
            //System.out.println("r:" + r);
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            //pw.println("{\"error\":\"" + e.getMessage().replace('"', '\'') + "\"}");
            return e.getMessage();
        }
    }

    /*
    public void sendImage(String filePath, HttpServletResponse response) {
        try {
            File f = new File(filePath);
            byte[] b = new byte[(int) f.length()];
            FileInputStream fis = new FileInputStream(f);
            fis.read(b);
            response.setContentType("image/jpeg");
            ServletOutputStream out = response.getOutputStream();
            out.write(b);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    */
}
