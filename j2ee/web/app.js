var canvas;
var context;
var iW = 0; // image width
var iH = 0; // image height
var p1 = 0.99;
var p2 = 0.99;
var p3 = 0.99;
var iBlurRate = 0;
var func = 'color'; // last used function

/*window.onload = function() {
    // creating context for original image
    canvasOrig = document.getElementById('orig');
    contextOrig = canvasOrig.getContext('2d');

    // draing original image
    var imgObj = new Image();
    imgObj.onload = function () {
        iW = this.width;
        iH = this.height;
        contextOrig.drawImage(imgObj, 0, 0, iW, iH); // draw the image on the canvas
    }
    imgObj.src = 'images/01.jpg';

    // creating testing context
    canvas = document.getElementById('panel');
    context = canvas.getContext('2d');
};*/

function Grayscale(imgd) {
    func = 'grayscale'; // last used function
    //var imgd = contextOrig.getImageData(0, 0, iW, iH);
    var data = imgd.data;

    for (var i = 0, n = data.length; i < n; i += 4) {
        var grayscale = data[i] * p1 + data[i+1] * p2 + data[i+2] * p3;
        data[i]   = grayscale; // red
        data[i+1] = grayscale; // green
        data[i+2] = grayscale; // blue
    }
    //context.putImageData(imgd, 0, 0);

}

function Blur(imgd) {
    func = 'blur'; // last used function
    //var imgd = contextOrig.getImageData(0, 0, iW, iH);
    var data = imgd.data;

    for (br = 0; br < iBlurRate; br += 1) {
        for (var i = 0, n = data.length; i < n; i += 4) {

            iMW = 4 * iW;
            iSumOpacity = iSumRed = iSumGreen = iSumBlue = 0;
            iCnt = 0;

            // data of close pixels (from all 8 surrounding pixels)
            aCloseData = [
                i - iMW - 4, i - iMW, i - iMW + 4, // top pixels
                i - 4, i + 4, // middle pixels
                i + iMW - 4, i + iMW, i + iMW + 4 // bottom pixels
            ];

            // calculating Sum value of all close pixels
            for (e = 0; e < aCloseData.length; e += 1) {
                if (aCloseData[e] >= 0 && aCloseData[e] <= data.length - 3) {
                    iSumOpacity += data[aCloseData[e]];
                    iSumRed += data[aCloseData[e] + 1];
                    iSumGreen += data[aCloseData[e] + 2];
                    iSumBlue += data[aCloseData[e] + 3];
                    iCnt += 1;
                }
            }

            // apply average values
            data[i] = (iSumOpacity / iCnt)*p1;
            data[i+1] = (iSumRed / iCnt)*p2;
            data[i+2] = (iSumGreen / iCnt)*p3;
            data[i+3] = (iSumBlue / iCnt);
        }
    }
    //context.putImageData(imgd, 0, 0);
}

function threshold(imgd, level) {
    var data = imgd.data;

    for (var i = 0, n = data.length; i < n; i += 4) {
        var grayscale = (data[i] + data[i+1] + data[i+2]) / 3.0;
        if (grayscale < level) {
            data[i] = 255;
            data[i + 1] = 255;
            data[i + 2] = 255;
        } else {
            data[i] = 0;
            data[i + 1] = 0;
            data[i + 2] = 0;
        }
    }
}

function resetToBlur(data, blurRate, _iw) {
    p1 = 1;
    p2 = 1;
    p3 = 1;
    iBlurRate = blurRate;
    iW = _iw;

    Blur(data);
}
function resetToGrayscale(data) {
    p1 = 0.3;
    p2 = 0.59;
    p3 = 0.11;
    iBlurRate = 0;

    Grayscale(data);
}