#include "algo3.h"

#include <fstream>
#include <iostream>
#include <math.h>
#include <string.h>

Mat img;

int image_index = 0;

int blur_level = 1;
int threshold_level = 1;
int blob_size_min = 1;
int blob_size_max = 1;
int aspect_ratio_min = 70; // 70%
int fill_ratio_min = 80;   // 80%

void save_parameters() {
    ofstream out;
    out.open("param.txt");
    if (!out.good()) {
        cout << "ERROR while writing parameters out" <<  endl;
        return;
    }
    cout << "SAVING:" << blur_level << ' ' << threshold_level << ' ' << blob_size_min << ' ' << blob_size_max << ' ' << aspect_ratio_min << ' ' << fill_ratio_min << endl;
    out               << blur_level << ' ' << threshold_level << ' ' << blob_size_min << ' ' << blob_size_max << ' ' << aspect_ratio_min << ' ' << fill_ratio_min << endl;

    out.close();
}

void read_parameters() {
    ifstream in;
    in.open("param.txt");
    if (!in.good()) {
        cout << "ERROR while reading parameters in" << endl;
        return;
    }

    in >> blur_level >> threshold_level >> blob_size_min >> blob_size_max >> aspect_ratio_min >> fill_ratio_min;
    cout << "READ:" << blur_level << ' ' << threshold_level << ' ' << blob_size_min << ' ' << blob_size_max << ' ' << aspect_ratio_min << ' ' << fill_ratio_min << endl;
    in.close();
}

vector<MyBlob> filterCandidatesFillRatio(const vector<MyBlob> &blobs) {
    vector<MyBlob> result;
    int n = blobs.size();
    for (int i = 0; i < n; i++) {
        const MyBlob &b = blobs[i];
        int intPercent = (100 * b.area) / b.rect.area();
        if (intPercent >= fill_ratio_min) {
            result.push_back(b);
        }
    }
    return result;
}

vector<MyBlob> filterCandidatesAspectRatio(const vector<MyBlob> &blobs) {
    vector<MyBlob> result;
    int n = blobs.size();
    for (int i = 0; i < n; i++) {
        const MyBlob &b = blobs[i];
        int intPercent = (100 * b.rect.width) / b.rect.height;
        if (intPercent >= aspect_ratio_min) {
            result.push_back(b);
        }
    }
    return result;
}

void updateImage() {
    if (blur_level > 0 && blob_size_min > 0 && blob_size_max) {
        Mat dst = filterImage(img, blur_level);
        threshold(dst, dst, threshold_level, 255, THRESH_BINARY);

        vector<MyBlob> blobs;
        getBlobs(dst, blobs);

        int factor = 100;

        int minArea = factor * blob_size_min;
        int maxArea = factor * blob_size_max;

        vector<MyBlob> preFilter;
        for (int i = 0; i < blobs.size(); i++) {
            if (minArea <= blobs[i].area && blobs[i].area <= maxArea) {
                preFilter.push_back(blobs[i]);
            } else {
                floodFill(dst, blobs[i].p, Scalar(224));
            }
        }

        vector<MyBlob> filtered1 = filterCandidatesFillRatio(preFilter);
        vector<MyBlob> filtered2 = filterCandidatesAspectRatio(filtered1);

        Mat res;
        cvtColor(dst, res, CV_GRAY2RGB);
        for (int i = 0; i < filtered2.size(); i++) {
            floodFill(res, filtered2[i].p, Scalar(0, 255, 0));
        }

        cout << filtered2.size() << " selected points" << endl;

        myShowImage(res, "Controls");
    }
}

void onTrackSlide(int p) {
    updateImage();
}

void onImageSlide(int p) {
    char a = image_index/10 + '0';
    char b = image_index%10 + '0';

    string filename = string("cases_negative/img") + a + string("") + b + string(".jpg");
    cout << "Loading " << filename << endl;
    //img = resizeToMatch(myLoadImage(filename, CV_LOAD_IMAGE_COLOR));
    img = resizeToMatch(myLoadImage(filename, CV_LOAD_IMAGE_GRAYSCALE));
    updateImage();
}

int process_algo3(int argc, char **argv) {

    read_parameters();

    cvNamedWindow("Controls", WINDOW_AUTOSIZE);

    cvCreateTrackbar("Threshold", "Controls", &threshold_level, 255, onTrackSlide);
    cvCreateTrackbar("Blur", "Controls", &blur_level, 12, onTrackSlide);
    cvCreateTrackbar("Blob size min", "Controls", &blob_size_min, 100, onTrackSlide);
    cvCreateTrackbar("Blob size max", "Controls", &blob_size_max, 100, onTrackSlide);
    cvCreateTrackbar("Aspect ratio min", "Controls", &aspect_ratio_min, 100, onTrackSlide);
    cvCreateTrackbar("Fill ratio min", "Controls", &fill_ratio_min, 100, onTrackSlide);

    cvCreateTrackbar("Image", "Controls", &image_index, 12, onImageSlide);


    onImageSlide(0);
    updateImage();

    if (waitKey(0) == 's') {
        save_parameters();
    }
    return 0;
}

