QT += core gui

CONFIG -= console

QMAKE_MAC_SDK = macosx10.11

TARGET = main
TEMPLATE = app

SOURCES += \
    cvlib.cpp \
    main.cpp \
    algo1.cpp \
    algo2.cpp \
    algo3.cpp

HEADERS += \
    cvlib.h \
    algo1.h \
    algo2.h \
    algo3.h

INCLUDEPATH += /usr/local/include

OTHER_FILES += \
    images/pic0.jpg \
    images/pic1.jpg \
    images/pic2.jpg \
    images/pic3.jpg \
    images/pic4.jpg \
    images/pic5.jpg \
    images/pic6.jpg \
    images/pic7.jpg \
    images/pic8.jpg \
    .gitignore \
    MyMakefile \
    param.txt

LIBS += -L/usr/local/lib \
    -lopencv_core \
    -lopencv_calib3d \
    -lopencv_features2d \
    -lopencv_contrib \
    -lopencv_flann \
    -lopencv_gpu \
    -lopencv_highgui \
    -lopencv_imgproc \
    -lopencv_legacy \
    -lopencv_ml \
    -lopencv_nonfree \
    -lopencv_objdetect \
    -lopencv_ocl \
    -lopencv_photo \
    -lopencv_stitching \
    -lopencv_superres \
    -lopencv_video \
    -lopencv_videostab
